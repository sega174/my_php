<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\assets\FontAwesomeAsset;
use frontend\assets\Bootstrap4Asset;

AppAsset::register($this);
FontAwesomeAsset::register($this);
Bootstrap4Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 image-header-block center-block" style="margin: 0px auto">
            <img src="/img/head.png" class="image-header hidden-xs">
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<?php
$menuItems = [
    ['label' => 'Главная', 'url' => ['/site/index']],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Регистрация', 'url' => ['/user/default/signup']];
    $menuItems[] = ['label' => 'Войти', 'url' => ['/user/default/login']];
} else {
    $menuItems[] = ['label' => 'Профиль', 'url' => ['/user/profile/view', 'nickname' => Yii::$app->user->identity->username]];
    $menuItems[] = ['label' => 'Создать пост', 'url' => ['/post/default/create']];
    $menuItems[] = ['label' => 'Выйти (' . Yii::$app->user->identity->username . ')', 'url' => ['/user/default/logout']];
}
echo Nav::widget([
    'options' => ['class' => 'nav nav-pills main-menu'],
    'items' => $menuItems,
]);
?>

<div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 center-block ">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    <div class="col-md-2"></div>

    <div class="push"></div>
</div>



<footer>
    <div class="footer">
        <div class="back-to-top-page">
            <a class="back-to-top"><i class="fa fa-angle-double-up"></i></a>
        </div>
        <p class="text"><a href="<?php echo Url::to(['/site/about']); ?>">Images | 2017</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<!-- Button trigger modal -->
