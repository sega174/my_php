<?php
/* @var $this yii\web\View */

/* @var $post common\models\Post */

use frontend\modules\post\assets\LikeAsset;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

LikeAsset::register($this);
?>
<?php
?>

<div class="block-post  row center-block">
    <div class="col-md-12 center-block">
        <div class="thumbnail">
            <div class="media" style="; padding-bottom: 10px;">
                <div style="display: inline-block">
                    <img src="<?= $post->getUser()->one()->getPicture(); ?>" class="mr-3 img-circle" alt="user"
                         style="width: 50px;">

                    <span class="mt-0 mb-1">
                      <a href="<?= Url::to(['/user/profile/view', 'nickname' => $post->getUser()->one()->getUserName()]); ?>">
                        <?php echo Html::encode($post->getUser()->one()->getUserName()); ?>
                          </a>
                    </span>
                </div>
            </div>


            <img src="<?php echo Yii::$app->storage->getFile($post->filename); ?>"
                 class="img-thumbnailcard-img-top" alt="cat" style="width: 100%;">
            <div class="caption">
                <ul class="list-unstyled" style="padding-bottom: 0px;margin-bottom: 5px;">
                    <li class="media">


                        <div class="col-md-12" style="text-align: center;">
                            <a href="#"
                               class="button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>"
                               data-id="<?php echo $post->id; ?>">
                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart"></span>
                            </a>
                            <a href="#"
                               class="button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>"
                               data-id="<?php echo $post->id; ?>">
                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart-empty"></span>
                            </a>

                           Лайков:  <span class="likes-count"><?php echo $post->countLikes(); ?></span>

                        </div>
                        <div class="media-body" style="padding-left: 15px; padding-top: 7px;">
                            <p class="card-text"><?php echo HtmlPurifier::process($post->description); ?>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>


<hr>

</div>

